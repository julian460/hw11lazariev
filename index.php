<?php
session_start();
if(!empty($_SESSION['selected'])){
    $selected = $_SESSION['selected'];
}
else{
    $selected = 'uah';
}
function getPriceWithDiscount($priceVal, $discountType, $discountVal ){
    if ($discountType === 'value') {
        $price = $priceVal - $discountVal;
    }else{
        $price = $priceVal - $priceVal*$discountVal;
    }
    return $price;
}
function convertPrice($price, $selected, $rate){
    foreach ($rate as $key => $value) {
        if ($selected === $key) {
            $finalPrice = round($price/$value['course'], 2) . ' ' . $value['name'];
            return $finalPrice;
        }
    }
}
$rate = [
    'uah'=>[
       'name' => 'Гривна',
       'course' => 1,
    ],
    'usd'=>[
       'name' => 'Доллар',
       'course' => 27.1,
    ],
    'eur'=> [
       'name' => 'Евро',
       'course' => 30.2,
    ],
 ];
 $items = [
    ['title' => 'Big TV', 'price_val' => 7500, 'discount_type' => 'value', 'discount_val' => 300],
    ['title' => 'Small TV', 'price_val' => 4687, 'discount_type' => 'value', 'discount_val' => 129],
    ['title' => 'Mouse', 'price_val' => 555, 'discount_type' => 'value', 'discount_val' => 32],
    ['title' => 'Router', 'price_val' => 699, 'discount_type' => 'value', 'discount_val' => 63],
    ['title' => 'Tablet PC', 'price_val' => 6432, 'discount_type' => 'value', 'discount_val' => 621],
    ['title' => 'Smartphone', 'price_val' => 11368, 'discount_type' => 'percent', 'discount_val' => 0.15],
    ['title' => 'PC', 'price_val' => 18634, 'discount_type' => 'percent', 'discount_val' => 0.08],
    ['title' => 'Laptop', 'price_val' => 23943, 'discount_type' => 'percent', 'discount_val' => 0.11],
    ['title' => 'Printer', 'price_val' => 3333, 'discount_type' => 'percent', 'discount_val' => 0.13],
    ['title' => 'Gamepad', 'price_val' => 999, 'discount_type' => 'percent', 'discount_val' => 0.23]
 ];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Here you can buy some items for personal use with good discount">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>My NEW shop</title>
</head>
<body>
<div class="conteiner" style="text-align: center">
    <h1>My shop!</h1>
</div>
<!-- Форма выбора валюты -->
<div class="container">
    <div class="row">
        <div class="col-3">
            <form action="/select.php" method="POST">
                <div class="form-control">
                    <div class="form-control">
                        <label>Выберите удобную валюту
                            <select name="selected" class="form-control">
                                <option selected value="uah">Гривна</option>
                                <option value="usd">Доллар</option>
                                <option value="eur">Евро</option>
                            </select>
                        </label>
                    </div>
                    <div class="form-control">        
                        <button class="btn btn-primary">Select</button>
                    </div>    
                </div>    
            </form>
        </div>
        <div class="col-9">
            <table>
                <tr>
                    <th>Товар</th>
                    <th>Цена</th>
                    <th>Цена с учетом скидки</th>
                </tr>
                <?php foreach ($items as $value):?>
                <tr>
                        <td><?=$value['title']?></th>
                        <td><?=convertPrice($value['price_val'], $selected, $rate)?></td>
                        <td><?=convertPrice(getPriceWithDiscount($value['price_val'], $value['discount_type'], $value['discount_val']), $selected, $rate)?></th>

                </tr>
                <?php endforeach;?>
            </table>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>    
</body>
</html>